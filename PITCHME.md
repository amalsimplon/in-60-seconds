# Algorithmique
## Tris dans les tableaux

---

## Qu'est-ce qu'un algorithme ?

---

@title[Qu'est-ce qu'un algorithme ?]
@ul 
- Un **algorithme** est la description précise, sous forme de **concepts simples**, de la manière dont on peut résoudre un problème.
- Un algorithme, c’est une **suite d’instructions**, qui une fois exécutée correctement, conduit à un **résultat donné**.


---

